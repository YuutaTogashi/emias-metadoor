FROM python:3.5

# Install debs
RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get install -y supervisor

# Install python libs
COPY requirements.pip /tmp/requirements.pip
RUN pip install -r /tmp/requirements.pip

# Install registrator
COPY ./src/metadoor /usr/local/lib/python3.5/site-packages/metadoor

# Prepare supervisord
COPY supervisord.conf /etc/supervisor/

# Prepare entrypoint
COPY ./entrypoint.sh /bin/metadoor
RUN chmod +x /bin/metadoor

# Prepare env
ENV PYTHONUNBUFFERED 1

CMD metadoor
