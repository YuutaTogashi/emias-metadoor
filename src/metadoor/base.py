# -*- coding: utf-8 -*-
import json
import logging
import os
import re
import threading
import time
import consul
import docker

from dict_tools import expand, get, merge

os.environ.setdefault('DOCKER_HOST', 'unix://var/run/docker.sock')

docker_cli = docker.APIClient(
    base_url=os.environ.get('DOCKER_HOST'),
    version=os.environ.get('DOCKER_API_VERSION', 'auto')
)
docker_events = docker_cli.events()

consul_cli = consul.Consul(host=os.environ.get('CONSUL_HOST') or 'consul')
consul_data_path = '/consul/data.d/{id}.json'



def check_fail(message):
    logging.warning(message)




def parse_env_value(text):
    if text == 'true' or text == 'True':
        text = True
    elif text == 'false' or text == 'False':
        text = False
    elif text == 'null':
        text = None
    elif len(text) == 1 or len(text) > 1 and text[0] != '0':
        try:
            text = int(text)
        except ValueError:
            try:
                text = float(text)
            except ValueError:
                pass
    return text


def extract_env(inspect):
    try:
        env = get(inspect, 'Config', 'Env', strict=True)
        result = {}

        for env_var in env:
            value = None
            key = env_var.split('=')[0]

            try:
                value = env_var.split('=')[1]
            except IndexError:
                value = 'null'

            value = parse_env_value(value)

            if key.startswith('CLUSTER_'):
                key = key.replace('CLUSTER_', '')
                sub_keys = key.lower().split('__')

                tmp = []
                for sub_key in sub_keys:
                    tmp.append(re.sub(r'(^|\s)(_+)(?=[a-zA-Z])', '', sub_key))
                sub_keys = tmp

                merge(result, expand(*sub_keys, value=value))
        return result

    except KeyError:
        return None
    except Exception as e:
        return {'exception': str(e)}


def event_action_kv(name, container_id, event=None):
    try:
        inspect = docker_cli.inspect_container(name)
    except docker.errors.NotFound as exception:
        inspect = {'exception': str(exception)}

    info_tuple = consul_cli.kv.get('production/docker/by_name/{name}'.format(name=name))
    info_dict = {}
    for content in info_tuple:
        # print(content)
        if not isinstance(content, dict):
            continue

        value = json.loads(content['Value'].decode('utf-8'))
        for existed_id in value:
            print(existed_id)
            info_dict[existed_id] = value[existed_id]

    if container_id not in info_dict:
        info_dict[container_id] = {}

    if event:
        info_dict[container_id]['event'] = event

    env = extract_env(inspect)
    if env:
        info_dict[container_id]['env'] = env

    info_dict[container_id]['inspect'] = inspect
    consul_cli.kv.put('production/docker/by_name/{name}'.format(name=name), json.dumps(info_dict, indent=2))
    consul_cli.kv.put('production/docker/by_id/{container_id}'.format(container_id=container_id),
                         json.dumps(info_dict[container_id], indent=2))


def listen_docker_events():
    for event in docker_events:
        event = json.loads(event.decode('utf-8'))

        try:
            name = get(event, 'Actor', 'Attributes', 'name', strict=True)
            action = get(event, 'Action', strict=True)
            container_id = get(event, 'id', strict=True)

            if not name or not action:
                continue
            if 'default' not in name:
                event_action_kv(name, container_id, event=event)

        except KeyError:
            pass


def catch_started_containers():
    containers = docker_cli.containers()

    for container in containers:
        names = get(container, 'Names')
        container_id = get(container, 'Id', strict=True)

        try:
            name = names[0].replace('/', '')
            event_action_kv(name, container_id)

        except Exception as exception:
            logging.error(str(exception))
