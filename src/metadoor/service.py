# -*- coding: utf-8 -*-
from . import base


if __name__ == '__main__':
    base.catch_started_containers()
    base.listen_docker_events()
