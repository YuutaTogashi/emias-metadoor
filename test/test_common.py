# -*- coding: utf-8 -*-
import json
import logging
import re
import signal
import subprocess
import time
import unittest
import consul
import docker

from dict_tools import expand, get, merge
from docker.errors import APIError



cli = docker.DockerClient(base_url='unix://var/run/docker.sock')
apicli = docker.APIClient(base_url='unix://var/run/docker.sock')
docker_events = cli.events()
test_container_name = 'test_docker_consul'
bashCommand = "docker-compose -f ../docker-compose.yml up --build"
passed = False

tmp_env = {
    'PYTHONUNBUFFERED': 1,
    'CLUSTER__DICT__INT': 1,
    'CLUSTER__DICT__DOUBLE': 1.0,
    'CLUSTER__DICT__STR': 'string',
    'CLUSTER__DICT__NULL': None,
    'CLUSTER__DICT__FALSE': False,
    'CLUSTER__DICT__TRUE': True,
    'CLUSTER_INT': 2,
    'CLUSTER_DOUBLE': 2.0,
    'CLUSTER_STR': 'string2',
    'CLUSTER_NULL': None,
    'CLUSTER_FALSE': False,
    'CLUSTER_TRUE': True
}


def get_expect_dict():
    expect_dict = {}

    for key in tmp_env:
        if key.startswith('CLUSTER_'):
            tmp_key = key.replace('CLUSTER_', '')
            sub_keys = tmp_key.lower().split('__')

            tmp = []
            for sub_key in sub_keys:
                tmp.append(re.sub(r'(^|\s)(_+)(?=[a-zA-Z])', '', sub_key))
            sub_keys = tmp

            merge(expect_dict, expand(*sub_keys, value=tmp_env[key]))
    return expect_dict


def start_test_container():

    test_container = cli.containers.run('python:3.5', environment=tmp_env
                                          , name=test_container_name
                                          , detach=True, tty=True)

    return test_container


class TestCommon(unittest.TestCase):
    def setUp(self):
        # try:
        #     apicli.remove_container('emiasregistrator_consul_1')
        #     apicli.remove_container('emiasregistrator_registrator_1')
        # except Exception:
        #     pass
        #
        # try:
        #     apicli.remove_volume('emiasregistrator_consul-data')
        # except APIError as ex:
        #     print(ex)
        #     pass
        self.test_container = start_test_container()
        self.process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)

    def tearDown(self):
        self.process.send_signal(signal.SIGINT)
        self.test_container.stop()
        self.test_container.remove()

    # def check_service(self, consul_client, service_id, repeat=20, timer=1, exist=True):
    #     while True:
    #         services = consul_client.agent.services()
    #         real_existing = service_id in services
    #         print('real: ', real_existing)
    #         if exist == real_existing:
    #             return
    #
    #         if repeat:
    #             repeat -= 1
    #             time.sleep(timer)
    #             continue
    #         else:
    #             msg = 'service with NAME:{0} and ID:{1} is not registered' if exist else \
    #                 'service with NAME:{0} and ID:{1} is not deregistered'
    #             msg = msg.format(get(services, service_id, 'Service'), service_id)
    #             logging.error(msg)
    #             self.assertTrue(False)

    def env_check(self, consul_client, expecting_env, name):
        counter = 5
        while counter:
            time.sleep(10)
            real_env = consul_client.kv.get('production/docker/by_name/{name}'.format(name=name))[1]

            if not real_env:
                counter -= 1
                continue
            real_env = json.loads(real_env['Value'].decode('utf-8'))[self.test_container.id]['env']
            if real_env != expecting_env:
                logging.error('KV env check not passed')
                self.assertTrue(False)
            else:
                return

    def inspect_event_check(self, consul_client, wait=1, repeat=5, event=None):
        expected_inspect = apicli.inspect_container(test_container_name)
        real_event = None
        repeat = int(repeat)
        if repeat < 0:
            return
        while repeat:
            time.sleep(wait)
            real_inspect = consul_client.kv.get('production/docker/by_name/{name}'.format(name=test_container_name))[1]
            if event:
                real_event = consul_client.kv.get('production/docker/by_name/{name}'.format(name=test_container_name))[1]

            if not real_inspect or (not real_event and event):
                repeat -= 1
                continue

            real_inspect = json.loads(real_inspect['Value'].decode('utf-8'))[self.test_container.id]['inspect']
            if event:
                real_event = json.loads(real_event['Value'].decode('utf-8'))[self.test_container.id]['event']
                self.assertEqual(event, real_event)

            self.assertEqual(expected_inspect, real_inspect)
            return

    def registration_test(self, consul_client, test_container, event=None):
        # self.check_service(consul_client, test_container.id)
        self.env_check(consul_client, get_expect_dict(), test_container_name)
        self.inspect_event_check(consul_client, event=event)
        apicli.kill(container=test_container.id)

    def deregistration_test(self, consul_client, test_container, last=False, event=None):
        # self.check_service(consul_client, test_container.id, exist=False)
        self.inspect_event_check(consul_client, event=event)
        if not last:
            apicli.start(container=test_container.id)
        else:
            self.assertTrue(True)
            raise KeyboardInterrupt

    def test_common(self):
        docker_monitor_started = False
        last_time = False

        for event in docker_events:
            event = json.loads(event.decode('utf-8'))

            try:
                name = get(event, 'Actor', 'Attributes', 'name', strict=True)
                action = get(event, 'Action', strict=True)

                if not name or not action:
                    continue
                if action == 'start':
                    print(name)
                    if 'metadoor_consul' in name:
                        time.sleep(5)
                        consul_client = consul.Consul(host='localhost')
                        self.registration_test(consul_client, self.test_container)
                        docker_monitor_started = True
                    if 'test_docker_consul' in name and docker_monitor_started:
                        consul_client = consul.Consul(host='localhost')
                        last_time = True
                        self.registration_test(consul_client,
                                               self.test_container,
                                               event=event)

                if action == 'die':
                    if 'test_docker_consul' in name:
                        consul_client = consul.Consul(host='localhost')
                        self.deregistration_test(consul_client,
                                                 self.test_container,
                                                 last=last_time,
                                                 event=event)

            except KeyError:
                pass
            except KeyboardInterrupt:
                return


if __name__ == '__main__':
    unittest.main(warnings=False)
