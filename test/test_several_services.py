# -*- coding: utf-8 -*-

import subprocess
import os
import re
import json
import requests
import base64
import unittest


path_to_consul_config = '../consul_conf.json'
consul_api_join = 'http://{receiver}:8500/v1/agent/join/{connected}'
consul_api_kv = 'http://{receiver}:8500/v1/kv/production/docker/by_name/{name}'
test_name_container = 'testing'
debug = True


def consul_kv_registered_ids(ip, name):
    r = requests.get(consul_api_kv.format(receiver=ip, name=name))
    response = json.loads(r.text)
    data = response[0]['Value']
    data = json.loads(base64.b64decode(data).decode('utf-8'))
    return data


def exec_local(cmd, **format):
    cmd = cmd.format(**format)
    if debug:
        print(cmd)
    result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
    if result.stderr:
        message = 'error: ' + result.stderr.decode('utf-8')
        if debug:
            print('-' * len(message))
            print(message)
            print('-' * len(message))

    result = result.stdout.decode('utf-8')
    if debug:
        print('out: ' + result)
    return result


def set_advertise(ip, name):
    consul_conf_file = open(path_to_consul_config)
    consul_conf = json.loads(consul_conf_file.read())
    consul_conf['advertise_addr'] = ip
    consul_conf['node_name'] = name

    consul_conf_file.close()
    consul_conf_file = open(path_to_consul_config, 'w')
    consul_conf_file.write(json.dumps(consul_conf, indent=2))
    consul_conf_file.close()


class Docker(object):
    def __init__(self, docker_machine):
        self.docker_machine = docker_machine

    def ps(self):
        return self.run('docker ps')

    def inspect(self, container):
        return self.run('docker inspect --format="{{{{.Id}}}}" {container}', container=container)

    def pull(self, name):
        return self.run('docker pull {name}'.format(name=name))

    def run(self, cmd, **format):
        self.docker_machine._activate()
        return exec_local(cmd, **format)


class DockerMachine(object):
    name = None
    docker = None
    meta = None

    def __init__(self, name):
        self.name = name
        self.docker = Docker(self)
        self.meta = dict()

        self.create()
        self.run()

    def create(self):
        return self.exec_local("docker-machine create --driver virtualbox {name}")

    def run(self):
        self._activate()
        self.meta['ip'] = self.exec_local("docker-machine ip {name}").strip()

    def sync_certs(self):
        self.ssh('mkdir -p /etc/docker/certs.d/')
        self.scp('/etc/docker/certs.d/')

    def scp(self, cmd):
        return self.exec_local('docker-machine scp -r {name}:/etc/docker/certs.d/ /etc/docker/certs.d/')

    def ssh(self, cmd):
        return self.exec_local('docker-machine ssh {name} sudo ' + cmd)

    def destroy(self):
        return self.exec_local("docker-machine rm {name} -y")

    def _activate(self):
        out = self.exec_local('docker-machine env {name}').split('\n')

        for env_line in out:
            match = re.search(r'([A-Z_])+', env_line)
            if not match:
                continue
            env_name = match.group(0).replace(r'\s', '')

            match = re.search(r'=.+', env_line)
            if not match:
                continue
            env_value = match.group(0)[1:].replace('"', '').replace(' ', '')
            os.environ[env_name] = env_value

    def exec_local(self, cmd):
        return exec_local(cmd, **self.__dict__)


class Cluster(object):
    nodes = None

    def __init__(self):
        self.nodes = dict()

    def create_node(self, name):
        if name not in self.nodes:
            self.nodes[name] = node = DockerMachine(name)
        else:
            raise TypeError()
        return node

    def destroy(self):
        for name, node in self.nodes.items():
            node.destroy()

    def run(self):
        for name, node in self.nodes.items():
            set_advertise(node.meta['ip'], name)

            node.docker.run('docker-compose -f ../docker-compose.yml up --build -d')

        for name, node in self.nodes.items():
            for tmp_name, tmp_node in self.nodes.items():
                if tmp_node.meta['ip'] != node.meta['ip']:
                    node.docker.run('docker exec -t metadoor_consul_1 consul join {ip}'
                                    .format(ip=tmp_node.meta['ip']))

    def start_test_containers(self):
        for name, node in self.nodes.items():
            node.docker.run('docker run -d --name {name} nginx'.format(name=test_name_container))

    def __getattr__(self, name):
        return self.nodes[name]


class TestSeveralServices(unittest.TestCase):
    def setUp(self):
        self.cluster = Cluster()

        self.cluster.create_node('n1')
        self.cluster.create_node('n2')
        self.cluster.create_node('n3')

        self.cluster.run()
        self.cluster.start_test_containers()
        self.ids = []

        for name, node in self.cluster.nodes.items():
            self.ids.append(node.docker.inspect(test_name_container).strip())

    def tearDown(self):
        self.cluster.destroy()
        pass

    def test_several_services(self):
        for name, node in self.cluster.nodes.items():
            ip = node.meta['ip']
            kv_data = consul_kv_registered_ids(ip, test_name_container)

            for key in kv_data:
                self.assertTrue(key in self.ids)


if __name__ == '__main__':
    unittest.main(warnings=False)
